/*
Cooper Adams
coopera
Lab 5
TA: Anurata Hridi
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;

enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  
  srand(unsigned (time(0)));

  /*The deck of cards is created and initialized*/

  Card deck[52];
 
  int sizeValue = 2;
 
  for(int i = 1; i < 14; ++i)
  {
	deck[i].value = sizeValue;
	deck[i].suit = SPADES;
	++sizeValue;
  }

  sizeValue = 2;

  for(int i = 14; i < 27; ++i)
  {
	deck[i].value = sizeValue;
	deck[i].suit = HEARTS;
	++sizeValue;
  }

  sizeValue = 2;

  for(int i = 27; i < 40; ++i)
  {
	deck[i].value = sizeValue;
	deck[i].suit = DIAMONDS;
	++sizeValue;
  }

  sizeValue = 2;

  for(int i = 40; i < 53; ++i)
  {
	deck[i].value = sizeValue;
	deck[i].suit = CLUBS;
	++sizeValue;
  } 

  /*After the deck is created and initialzed we call random_shuffle()*/

  std::random_shuffle(deck + 1, deck + 52, myrandom);

  /*A hand is then built from the first 5 cards of the shuffled deck*/

  Card hand[5] = {deck[1], deck[2], deck[3], deck[4], deck[5]};

  /*The hand is then passed to the sort function which sorts by suit
  then by value*/

  sort(hand, hand + 5, suit_order);

  /*The hand is then printed to the terminal with a width of 10 spaces
  and is right-aligned*/

  for(int i = 0; i < 5; ++i)
  {
	string name = get_card_name(hand[i]);
	string suitType = get_suit_code(hand[i]);
	std::cout.width(10); std::cout << right << name << " of " << suitType << '\n';
  }

  return 0;
}

bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit) {
    return true;
  } 

  else if (lhs.suit == rhs.suit) {
    return lhs.value < rhs.value;
  }
  
  return false;   
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  if (c.value == 11)
  {
	return "Jack";
  }

  else if (c.value == 12)
  {
	return "Queen";
  }

  else if (c.value == 13)
  {
	return "King";
  }

  else if (c.value == 14)
  {
	return "Ace";
  }

  else
  {
	return to_string(c.value);
  }
}
